import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import {MainContentGuardian} from "./guardians/main-content-guardian";
import { AuthServiceService } from "./shared-services/auth-service/auth-service.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: 'login', loadChildren: 'app/modules/login/login.module#LoginModule', data: { isRouteLogin: true }, canActivate: [MainContentGuardian] },
      { path: 'service', loadChildren: 'app/modules/main-content/main-content.module#MainContentModule', canActivate: [MainContentGuardian], data: { isRouteLogin: false } },
      { path: '', redirectTo: 'service', pathMatch: 'full' },
      { path: '**', redirectTo: 'service', pathMatch: 'full' }
    ]),
  ],

  providers: [MainContentGuardian, AuthServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
