import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import {AuthServiceService} from "../shared-services/auth-service/auth-service.service";

@Injectable()
export class MainContentGuardian implements CanActivate {

  constructor(private router: Router, private auth: AuthServiceService) {

  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    var isLogin = route.data["isRouteLogin"] as boolean;
    if (isLogin && this.auth.checkExistToken()) {
      this.router.navigate(['service']);
      return false;
    }
    if (!this.auth.checkExistToken() && !isLogin) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
