import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import {AuthServiceService} from "../auth-service/auth-service.service";
import {environment} from "../../../environments/environment";

@Injectable()
export class HttpBaseService {
  constructor(private http: Http,private auth: AuthServiceService) { }
  get(url: string) {

    return this.http.get(`${environment.serverHost }/${url}`, this.getrRequestOption()).map(x => x.json());
  }

  delete(url: string) {
    return this.http.delete(`${environment.serverHost}/${url}`, this.getrRequestOption()).map(x => x.json());
  }
  post(url: string, model: any) {
    var requests = this.getrRequestOption();
    requests.headers.append("content-type", "application/json");
    return this.http.post(`${environment.serverHost}/${url}`, JSON.stringify(model), requests).map(x => x.json());
  }
  put(url: string, obj: any) {
    var requests = this.getrRequestOption();
    requests.headers.append("content-type", "application/json");
    return this.http.put(`${environment.serverHost}/${url}`, JSON.stringify(obj), requests).map(x => x.json());
  }
  private getrRequestOption() {
    let header = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
    return  new RequestOptions({ headers: header });
  }

  serialize(obj: any):string {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }
}
