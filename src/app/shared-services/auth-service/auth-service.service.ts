import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TokenResult } from "../../shared-models/TokenResult";
import 'rxjs/add/operator/map';
import { environment } from "../../../environments/environment";

@Injectable()
export class AuthServiceService {
  protected static tokenResult: TokenResult;
  constructor(private http: Http) {
    AuthServiceService.tokenResult = TokenResult.checkExistsTokenAndNotExpired();
  }
  public requestToken(login: string, password: string): Observable<boolean> {
    var data = "grant_type=password&username=" + login + "&password=" + password;
    return this.http.post(environment.serverHost + "/token",
      data,
      new RequestOptions({ headers: this.getHeaders()})).map(
      (res) => res.json()).map((res) => {
        if (res.Error || res.Error)
          return false;
        return this.setTokenParams(res);
      }, (res) => false);
  }
  private setTokenParams(res: any): boolean {
    AuthServiceService.tokenResult = new TokenResult(res);
    return true;
  }
  public getToken():string {
    return AuthServiceService.tokenResult.access_token;
  }
  private getHeaders(): Headers {
    var hars = new Headers();
    hars.append("Content-Type", 'application/x-www-form-urlencoded');
    return hars;
  }
  public checkExistToken(): boolean {
    return (AuthServiceService.tokenResult && TokenResult.checkTokenExpires(AuthServiceService.tokenResult));
  }
  public getUserName(): string {
    return AuthServiceService.tokenResult.userName;
  }
}
