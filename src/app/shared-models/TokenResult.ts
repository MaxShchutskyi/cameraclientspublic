import * as moment from "moment";
export class TokenResult {
  access_token;
  token_type;
  expires_in;
  userName;
  issued;
  expires;
  private static tokenName = "token"; 

  constructor(obj:any) {
    this.access_token = obj.access_token;
    this.token_type = obj.token_type;
    this.expires_in = obj.expires_in;
    this.userName = obj.userName;
    this.issued = obj[".issued"];
    this.expires = obj[".expires"];
    this.setTokenInStorage();
  }
  public static checkTokenExpires(token: TokenResult): boolean {
    var tk = moment.utc(token.expires, "DD-MM-YYYY HH:mm:ss").local();
    //var ss = moment.utc().toDate();
    //debugger;
    return tk > moment.utc();
  }
  private setTokenInStorage() {
    localStorage.setItem(TokenResult.tokenName, JSON.stringify(this));

  }
  public static checkExistsTokenAndNotExpired(): TokenResult {
    if (!localStorage.getItem(TokenResult.tokenName)) return null;
    var tk = JSON.parse(localStorage.getItem(TokenResult.tokenName));
    if (!TokenResult.checkTokenExpires(tk))
      return null;
    return tk;

  }
}
