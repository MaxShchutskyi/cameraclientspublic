import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Page } from 'app/modules/plates-page/models/page';
import { MatDialog } from '@angular/material';
import {AllVideosServiceService} from "../../services/all-videos-service/all-videos-service.service";
import {AllVideosShowVideosComponent} from "../all-videos-show-videos/all-videos-show-videos.component";
import * as moment from "moment";
@Component({
  selector: 'app-all-videos-main-cintent',
  templateUrl: './all-videos-main-cintent.component.html',
  styleUrls: ['./all-videos-main-cintent.component.scss']
})
export class AllVideosMainCintentComponent implements OnInit {
  page: Page = new Page();
  modelChanged: Subject<string> = new Subject<string>();
  typesOfSelect: Array<any> = [{ name: 'All fields' }, { name: "Dates" }];
  dateFrom;
  dateTo;
  rows;
  constructor(private allVideosServiceService: AllVideosServiceService, public dialog: MatDialog) {
    this.page.pageNumber = 0;
    this.page.size = 9;
    this.page.filterType = "All fields";
    this.modelChanged
      .debounceTime(800)
      .distinctUntilChanged()
      .subscribe(val => {
        console.log(this.page.filterValue);
        this.page.filterValue = val;
        this.page.dateFrom = "";
        this.page.dateTo = "";
        this.page.changedFilterValue = true;
        this.setPage({ offset: 0 });
      });
  }
  changeType() {
    this.page.filterValue = "";
    this.page.changedFilterValue = true;
    this.page.dateFrom = "";
    this.page.dateTo = "";
    this.setPage({ offset: 0 });
  }
  textChanged(text) {
    this.modelChanged.next(text);
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.page.changedPage = true;
    this.setData();
  }
  setData() {
    this.allVideosServiceService.getAllVideos(this.page).subscribe(res => {
      debugger;
      this.rows = res.videos;
      this.page.copyFields(res.page);
      this.page.cleanAllBooleanValues();
    });
  }
  openDialog(data) {
    debugger;
    this.dialog.open(AllVideosShowVideosComponent, {
      width: '600px',
      data: data
    });
  }
  fltr() {
    this.page.filterValue = "";
    this.page.changedFilterValue = true;
    this.page.dateFrom = moment(this.dateFrom).format();
    this.page.dateTo = moment(this.dateTo).format();
    debugger;
    this.setPage({ offset: 0 });
  }
  ngOnInit() {
    this.setPage({ offset: 0 });
  }

}
