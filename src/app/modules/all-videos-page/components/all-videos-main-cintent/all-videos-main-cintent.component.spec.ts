import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllVideosMainCintentComponent } from './all-videos-main-cintent.component';

describe('AllVideosMainCintentComponent', () => {
  let component: AllVideosMainCintentComponent;
  let fixture: ComponentFixture<AllVideosMainCintentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllVideosMainCintentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllVideosMainCintentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
