import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllVideosShowVideosComponent } from './all-videos-show-videos.component';

describe('AllVideosShowVideosComponent', () => {
  let component: AllVideosShowVideosComponent;
  let fixture: ComponentFixture<AllVideosShowVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllVideosShowVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllVideosShowVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
