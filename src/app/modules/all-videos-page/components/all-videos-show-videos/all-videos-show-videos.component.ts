import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-all-videos-show-videos',
  templateUrl: './all-videos-show-videos.component.html',
  styleUrls: ['./all-videos-show-videos.component.scss']
})
export class AllVideosShowVideosComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<AllVideosShowVideosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  ngOnInit() {
  }

}
