import { Injectable } from '@angular/core';
import {HttpBaseService} from "../../../../shared-services/http-base/http-base.service";

@Injectable()
export class AllVideosServiceService {

  constructor(private base: HttpBaseService) { }
  getAllVideos(page) {
    var query = this.base.serialize(page);
    return this.base.get(`api/allvideos?${query}`);
  }

}
