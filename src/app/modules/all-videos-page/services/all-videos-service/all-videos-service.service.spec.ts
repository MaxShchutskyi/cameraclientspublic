import { TestBed, inject } from '@angular/core/testing';

import { AllVideosServiceService } from './all-videos-service.service';

describe('AllVideosServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllVideosServiceService]
    });
  });

  it('should be created', inject([AllVideosServiceService], (service: AllVideosServiceService) => {
    expect(service).toBeTruthy();
  }));
});
