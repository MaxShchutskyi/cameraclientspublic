import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AllVideosMainCintentComponent} from "./components/all-videos-main-cintent/all-videos-main-cintent.component";

const routes: Routes = [{ path: '', component: AllVideosMainCintentComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllVideosPageRoutingModule { }
