import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllVideosPageRoutingModule } from './all-videos-page-routing.module';
import { AllVideosMainCintentComponent } from './components/all-videos-main-cintent/all-videos-main-cintent.component';
import { AllVideosShowVideosComponent } from './components/all-videos-show-videos/all-videos-show-videos.component';
import {AllVideosServiceService} from "./services/all-videos-service/all-videos-service.service";
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatDialogModule, MatInputModule, MatFormFieldModule, MatNativeDateModule, MatDatepickerModule, MAT_DATE_LOCALE} from '@angular/material';
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    NgSelectModule,
    NgxDatatableModule,
    MatDatepickerModule, MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    AllVideosPageRoutingModule
  ],
  declarations: [AllVideosMainCintentComponent, AllVideosShowVideosComponent],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'en-GB' },AllVideosServiceService],
  entryComponents: [AllVideosShowVideosComponent]
})
export class AllVideosPageModule { }
