import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import {AuthServiceService} from "../../../../shared-services/auth-service/auth-service.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgZone } from '@angular/core';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.scss']
})
export class LoginComponentComponent implements OnInit {
  languages: Array<any>;
  group:FormGroup;
  constructor(private builder: FormBuilder, private auth: AuthServiceService, private router: Router, zone: NgZone) { }
  ngOnInit() {

    this.group = this.builder.group({
      'login': [''],
      'password': [''],
      'language':['en']
    });
    this.languages = [{ name: 'English', value: 'en' }, { name: 'Hebrew', value: 'il' } ];
  }
  sendRequestToken() {
    var model = this.group.value;
    this.auth.requestToken(model.login, model.password).subscribe(res => {
      if (res)
        this.router.navigate(["/service"]);
      else
        alert("Wront Username or Password");
    }, err => {
      alert("Wront Username or Password");
    });
  }

}
