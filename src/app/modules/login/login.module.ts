import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';
import {LoginComponentComponent} from "./components/login-component/login-component.component";
import { AuthServiceService } from "../../shared-services/auth-service/auth-service.service";
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    NgSelectModule,
    LoginRoutingModule,
    ReactiveFormsModule
  ],
  providers: [AuthServiceService],
  declarations: [LoginComponentComponent]
})
export class LoginModule { }
