import { Component, OnInit } from '@angular/core';
import * as moment from "moment"
import {ChartsServiceService} from "../../services/charts-service/charts-service.service";

@Component({
  selector: 'app-charts-main-component',
  templateUrl: './charts-main-component.component.html',
  styleUrls: ['./charts-main-component.component.scss']
})
export class ChartsMainComponentComponent implements OnInit {
  constructor(private chartsServiceService: ChartsServiceService) {  }
  colorScheme = {
    domain: ['#fe780e', '#fe444e']
  };
  data = [];
  data2 = [];
  ngOnInit() {
    this.chartsServiceService.getChart("0").subscribe(res => {
      if (!res) return;
      res.series.forEach(x => x.name = new Date(x.name));
      this.data.push(res);
    });
    //this.data.push({
    //  name: "Main",
    //  series: []
    //});
    //this.data2.push({
    //  name: "Main",
    //  series: []
    //});
    //for (var i = 1; i < 31; i++) {
    //  this.data[0].series.push({ name: new Date(2018, 1, i), value: this.getRandomInt(0, 5000) });
    //  //console.log(new Date(2018, 1, i).toISOString());
    //}
    //for (var i = 1; i < 31; i++) {
    //  this.data2[0].series.push({ name: new Date(2018, 1, i), value: this.getRandomInt(0, 5000) });
    //  //console.log(new Date(2018, 1, i).toISOString());
    //}
};
  //getRandomInt(min, max) {
  //  return Math.floor(Math.random() * (max - min)) + min;
  //}
}
