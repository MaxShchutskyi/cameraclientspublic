import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsMainComponentComponent } from './charts-main-component.component';

describe('ChartsMainComponentComponent', () => {
  let component: ChartsMainComponentComponent;
  let fixture: ComponentFixture<ChartsMainComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsMainComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsMainComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
