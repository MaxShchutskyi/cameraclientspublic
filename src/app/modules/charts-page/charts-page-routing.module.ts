import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ChartsMainComponentComponent} from "./components/charts-main-component/charts-main-component.component";

const routes: Routes = [{ path: '', component: ChartsMainComponentComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartsPageRoutingModule { }
