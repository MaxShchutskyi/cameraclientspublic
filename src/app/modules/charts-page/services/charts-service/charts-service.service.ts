import { Injectable } from '@angular/core';
import { HttpBaseService } from 'app/shared-services/http-base/http-base.service';

@Injectable()
export class ChartsServiceService {

  constructor(private base:HttpBaseService) { }
  getChart(type: string) {
    return this.base.get("api/charts?type=" + type);
  }
}
