import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartsPageRoutingModule } from './charts-page-routing.module';
import { ChartsMainComponentComponent } from './components/charts-main-component/charts-main-component.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {ChartsServiceService} from "./services/charts-service/charts-service.service";

@NgModule({
  imports: [
    CommonModule,
    NgxChartsModule,
    ChartsPageRoutingModule
  ],
  declarations: [ChartsMainComponentComponent],
  providers: [ChartsServiceService]
})
export class ChartsPageModule { }
