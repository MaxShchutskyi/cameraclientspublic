export class MainModelOfTopContent {
  mainIndicator: number;
  percent: number;
  diaposonDates: string;
  nameOfTheBlock: string;

  constructor(obj:any) {
    Object.assign(this, obj);
  }
}
