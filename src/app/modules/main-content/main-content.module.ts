import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainContentRoutingModule } from './main-content-routing.module';
import { MainComponentComponent } from './components/main-component/main-component.component';
import {HeaderComponentComponent} from "./components/header-component/header-component.component";
import {AuthServiceService} from "../../shared-services/auth-service/auth-service.service";
import { TopCardComponentComponent } from './components/top-card-component/top-card-component.component';
import { MainContentComponentComponent } from './components/main-content-component/main-content-component.component';
import { RightWidgetComponentComponent } from './components/right-widget-component/right-widget-component.component';
import { FormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatTabsModule } from '@angular/material/tabs';
import { NgSelectModule } from '@ng-select/ng-select';
import {HttpBaseService} from "../../shared-services/http-base/http-base.service";
import {StatisticServiceService} from "./services/statistic-service/statistic-service.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgSelectModule,
    MatTabsModule,
    NgxChartsModule,
    MainContentRoutingModule
  ],
  declarations: [MainComponentComponent, HeaderComponentComponent, TopCardComponentComponent, MainContentComponentComponent, RightWidgetComponentComponent],
  providers: [AuthServiceService, HttpBaseService, StatisticServiceService]
})
export class MainContentModule { }
