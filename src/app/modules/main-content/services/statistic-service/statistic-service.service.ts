import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {MainModelOfTopContent} from "../../models/MainModelOfTopContent";
import { Observable } from 'rxjs/Observable';
import {environment} from "../../../../../environments/environment";
import {HttpBaseService} from "../../../../shared-services/http-base/http-base.service";

@Injectable()
export class StatisticServiceService {
  constructor(private baseService: HttpBaseService) {  }
  getStatisticByType(type: string): Observable<MainModelOfTopContent> {
    var url = `api/statistic?type=${type}`;
    return this.baseService.get(url).map(x => new MainModelOfTopContent(x));
  }
  getBarStatisticByType(type: string): Observable<MainModelOfTopContent> {
    var url = `api/barstatistic?type=${type}`;
    return this.baseService.get(url).map(x => new MainModelOfTopContent(x));
  }
}
