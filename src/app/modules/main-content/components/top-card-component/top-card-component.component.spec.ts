import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopCardComponentComponent } from './top-card-component.component';

describe('TopCardComponentComponent', () => {
  let component: TopCardComponentComponent;
  let fixture: ComponentFixture<TopCardComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopCardComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopCardComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
