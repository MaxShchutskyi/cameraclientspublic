import { Component, OnInit, Input } from '@angular/core';
import {StatisticServiceService} from "../../services/statistic-service/statistic-service.service";
import {MainModelOfTopContent} from "../../models/MainModelOfTopContent";

@Component({
  selector: 'app-top-card-component',
  templateUrl: './top-card-component.component.html',
  styleUrls: ['./top-card-component.component.scss']
})
export class TopCardComponentComponent implements OnInit {
  @Input() type;
  @Input() intervalUpdate;
  public mainModelOfTopContent: MainModelOfTopContent;
  constructor(private statisticServiceService: StatisticServiceService) { }
  intervalId:any;
  ngOnInit() {
    this.setData();
  }
  ngAfterViewInit() {
    if (this.intervalUpdate)
      this.intervalId = setInterval(() => this.setData(), parseInt(this.intervalUpdate));
  }
  setData() {
    this.statisticServiceService.getStatisticByType(this.type).subscribe(x => {
      this.mainModelOfTopContent = x;
    });
  }
}
