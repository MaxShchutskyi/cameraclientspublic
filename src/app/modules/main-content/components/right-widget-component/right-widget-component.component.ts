import { Component, OnInit } from '@angular/core';
import {StatisticServiceService} from "../../services/statistic-service/statistic-service.service";

@Component({
  selector: 'app-right-widget-component',
  templateUrl: './right-widget-component.component.html',
  styleUrls: ['./right-widget-component.component.scss']
})
export class RightWidgetComponentComponent implements OnInit {
  items: Array<any>;
  totalPlates:string;
  colorScheme = {
    domain: ['#47c1bf']
  };
  data = [];
  onSelect(ev) {

  }
  constructor(private statisticServiceService: StatisticServiceService) { }
  selectedItem: string = "This month";
  getData() {
    var type;
    switch (this.selectedItem) {
      case "This month":
        type = 0;
        break;
      case "Last week":
        type = 1;
        break;
      case "This year":
        type = 2;
        break;
      case "Last year":
        type = 3;
        break;
    default:
      return null;
    }
    this.statisticServiceService.getBarStatisticByType(type).subscribe(res => {
      this.data = res["results"];
      this.totalPlates = res["countOfPlates"].toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    });
  }
  ngOnInit() {
    this.items = [{ name: 'This month' }, { name: 'Last week' }, { name: 'This year' }, {name:'Last year'}];
    this.getData();
  }
  changeMd() {
    this.getData();
  }
}
