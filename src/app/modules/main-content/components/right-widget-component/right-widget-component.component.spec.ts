import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightWidgetComponentComponent } from './right-widget-component.component';

describe('RightWidgetComponentComponent', () => {
  let component: RightWidgetComponentComponent;
  let fixture: ComponentFixture<RightWidgetComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightWidgetComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightWidgetComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
