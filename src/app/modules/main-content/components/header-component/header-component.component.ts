import { Component, OnInit } from '@angular/core';
import {AuthServiceService} from "../../../../shared-services/auth-service/auth-service.service";

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.scss']
})
export class HeaderComponentComponent implements OnInit {
  userName:string;
  constructor(private auth: AuthServiceService) { }

  ngOnInit() {
    this.userName = this.auth.getUserName();
  }

}
