import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponentComponent} from "./components/main-component/main-component.component";

const routes: Routes = [
  {
    path: '',
    component: MainComponentComponent,
    children: [
      {
        path: 'plates',
        loadChildren: "app/modules/plates-page/plates-page.module#PlatesPageModule"
      },
      {
        path: 'cameras',
        loadChildren: "app/modules/cameras-page/cameras-page.module#CamerasPageModule"
      },
      {
        path: 'charts',
        loadChildren: "app/modules/charts-page/charts-page.module#ChartsPageModule"
      },
      {
        path: 'black-list',
        loadChildren: "app/modules/white-numbers-page/white-numbers-page.module#WhiteNumbersPageModule"
      },
      {
        path: 'all-videos',
        loadChildren: "app/modules/all-videos-page/all-videos-page.module#AllVideosPageModule"
      },
      {
        path: '',
        redirectTo:'plates'
      },
      {
        path: '**',
        redirectTo: 'plates'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainContentRoutingModule { }
