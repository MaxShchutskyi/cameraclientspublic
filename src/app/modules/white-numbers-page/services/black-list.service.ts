import { Injectable } from '@angular/core';
import { HttpBaseService } from 'app/shared-services/http-base/http-base.service';

@Injectable()
export class BlackListService {

  constructor(private baseService: HttpBaseService) { }
  getAllPlates() {
    return this.baseService.get("api/blacklist");
  }
  delete(id) {
    return this.baseService.delete(`delete/${id}`);
  }
  addUpdate(model) {
    return this.baseService.post("api/blacklist", model);
  }
}
