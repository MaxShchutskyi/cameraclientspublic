import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhiteListMainComponentComponent } from './white-list-main-component.component';

describe('WhiteListMainComponentComponent', () => {
  let component: WhiteListMainComponentComponent;
  let fixture: ComponentFixture<WhiteListMainComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhiteListMainComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteListMainComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
