import { Component, OnInit } from '@angular/core';
import { BlackListService } from 'app/modules/white-numbers-page/services/black-list.service';
import { MatDialog } from '@angular/material';
import { AddUpdatePlateComponent } from 'app/modules/white-numbers-page/components/modals/add-update-plate/add-update-plate.component';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-white-list-main-component',
  templateUrl: './white-list-main-component.component.html',
  styleUrls: ['./white-list-main-component.component.scss']
})
export class WhiteListMainComponentComponent{

  typesOfSelect: Array<any> = [{ name: 'All fields' }];
  type: string = 'All fields';
  rows;
  filter = "";
  constructor(private blackListService: BlackListService, public dialog: MatDialog) {
    blackListService.getAllPlates().subscribe(res => {
      console.log(res);
      this.rows = res
    });
  }
  addNew() {
    var dialog = this.dialog.open(AddUpdatePlateComponent, {
      width: '400px',
      data: null
    });
    dialog.afterClosed().mergeMap(res => {
      if (!res)
        return Observable.empty();
      return this.blackListService.addUpdate(res);
    })
      .subscribe(res => {
        this.rows = res;
      });
  }
  getRows() {
    if (!this.rows || !this.rows.length)
      return [];
    return this.rows.filter(x => x.plate.indexOf(this.filter) > -1);
  }
  deleteRow(row) {
    this.blackListService.delete(row.id).subscribe(res => this.rows = res);
  }
  editRow(row) {
    var dialog = this.dialog.open(AddUpdatePlateComponent, {
      width: '400px',
      data: row
    });
    dialog.afterClosed()
      .mergeMap(res => {
        if (!res)
          return Observable.empty();
        return this.blackListService.addUpdate(res);
      })
      .subscribe(res => {
        this.rows = res;
      });
  }
}
