import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdatePlateComponent } from './add-update-plate.component';

describe('AddUpdatePlateComponent', () => {
  let component: AddUpdatePlateComponent;
  let fixture: ComponentFixture<AddUpdatePlateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpdatePlateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdatePlateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
