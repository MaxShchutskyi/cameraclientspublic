import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-update-plate',
  templateUrl: './add-update-plate.component.html',
  styleUrls: ['./add-update-plate.component.scss']
})
export class AddUpdatePlateComponent implements OnInit {
  frmGroup: FormGroup;
  constructor(private builder: FormBuilder, public dialogRef: MatDialogRef<AddUpdatePlateComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit() {
    this.frmGroup = this.builder.group({
      'id': new FormControl('0'),
      'plate': new FormControl('', [Validators.required])
    });
    if (this.data) {
      this.frmGroup.get('plate').setValue(this.data.plate);
      this.frmGroup.get('id').setValue(this.data.id);
    }
  }
  saveChanges() {
    this.dialogRef.close(this.frmGroup.getRawValue());
  }

}
