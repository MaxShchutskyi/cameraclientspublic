import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WhiteNumbersPageRoutingModule } from './white-numbers-page-routing.module';
import { WhiteListMainComponentComponent } from './components/white-list-main-component/white-list-main-component.component';

import { MatButtonModule, MatDialogModule } from "@angular/material"
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlackListService } from 'app/modules/white-numbers-page/services/black-list.service';
import { AddUpdatePlateComponent } from './components/modals/add-update-plate/add-update-plate.component';
@NgModule({
  imports: [
    CommonModule,
    NgSelectModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    MatDialogModule,
    FormsModule,
    MatButtonModule,
    WhiteNumbersPageRoutingModule
  ],
  declarations: [WhiteListMainComponentComponent, AddUpdatePlateComponent],
  providers: [BlackListService],
  entryComponents: [AddUpdatePlateComponent]
})
export class WhiteNumbersPageModule { }
