import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WhiteListMainComponentComponent} from
  "./components/white-list-main-component/white-list-main-component.component";

const routes: Routes = [
  {
    path: '', component: WhiteListMainComponentComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WhiteNumbersPageRoutingModule { }
