import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CamerasMainComponentComponent} from "./components/cameras-main-component/cameras-main-component.component";

const routes: Routes = [{ path: '', component: CamerasMainComponentComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CamerasPageRoutingModule { }
