export class Camera {
  id: number;
  cameraName: string;
  ip: string;
  port: number;
  login: string;
  password: string;

  constructor(obj:any) {
    Object.assign(this, obj);
  }
  copyFields(obj: any) {
    Object.assign(this, obj);
  }
}
