import { AbstractControl } from "@angular/forms";
import {Camera} from "../models/camera";


export const ipCameraExist = (cameras: Array<Camera>) => {
  return (control: AbstractControl) => {
    if (cameras.find(x => x.ip === control.value)){
      return {
        ipExist: {valid: false}
      };
    }
    return null;
  };
};
export const ipNameExist = (cameras: Array<Camera>) => {
  return (control: AbstractControl) => {
    if (cameras.find(x => x.cameraName === control.value)) {
      return {
        nameExist: { valid: false }
      };
    }
    return null;
  };
};

