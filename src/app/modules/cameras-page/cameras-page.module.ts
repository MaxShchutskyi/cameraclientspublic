import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CamerasPageRoutingModule } from './cameras-page-routing.module';
import { CamerasMainComponentComponent } from './components/cameras-main-component/cameras-main-component.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CameraManagerServiceService} from "./services/camera-manager-service/camera-manager-service.service";
import { CameraEditCameraComponentComponent } from './components/camera-edit-camera-component/camera-edit-camera-component.component';
import { MatDialogModule } from '@angular/material';

@NgModule({
  imports: [
    NgSelectModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    CommonModule,
    CamerasPageRoutingModule
  ],
  declarations: [CamerasMainComponentComponent, CameraEditCameraComponentComponent],
  providers: [CameraManagerServiceService],
  entryComponents: [CameraEditCameraComponentComponent]
})
export class CamerasPageModule { }
