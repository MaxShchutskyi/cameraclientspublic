import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamerasMainComponentComponent } from './cameras-main-component.component';

describe('CamerasMainComponentComponent', () => {
  let component: CamerasMainComponentComponent;
  let fixture: ComponentFixture<CamerasMainComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamerasMainComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamerasMainComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
