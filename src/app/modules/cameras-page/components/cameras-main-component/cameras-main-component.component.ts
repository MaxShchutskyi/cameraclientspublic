import { Component, OnInit } from '@angular/core';
import {Camera} from "../../models/camera";
import {CameraManagerServiceService} from "../../services/camera-manager-service/camera-manager-service.service";
import {MatDialog} from "@angular/material";
import {CameraEditCameraComponentComponent} from
  "../camera-edit-camera-component/camera-edit-camera-component.component";

@Component({
  selector: 'app-cameras-main-component',
  templateUrl: './cameras-main-component.component.html',
  styleUrls: ['./cameras-main-component.component.scss']
})
export class CamerasMainComponentComponent {
  typesOfSelect: Array<any> = [{ name: 'All fields' }];
  type: string = 'All fields';
  rows;
  constructor(private cameraManagerServiceService: CameraManagerServiceService, public dialog: MatDialog) {
  }
  editCamera(id) {
    var camera = this.rows.find(x => x.id === id);
    var dialog = this.dialog.open(CameraEditCameraComponentComponent, {
      width: '400px',
      data: camera
    });
    dialog.afterClosed().mergeMap(res => {
      if (!res) return null;
      var dt = this.rows.find(x => x.id === res.id);
      Object.assign(dt, res);
      return this.cameraManagerServiceService.updateCamera(dt);
    }).subscribe(x => {
      debugger;
    });
  }
  openTab(row) {
    window.open(`http://${row.login}:${row.password}@${row.ip}:${row.port}/Streaming/channels/2/httppreview`,'_blank');
  }
  ngOnInit() {
    this.cameraManagerServiceService.getAllCameras().subscribe(res => this.rows = res);
  }
}
