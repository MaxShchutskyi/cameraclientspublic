import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraEditCameraComponentComponent } from './camera-edit-camera-component.component';

describe('CameraEditCameraComponentComponent', () => {
  let component: CameraEditCameraComponentComponent;
  let fixture: ComponentFixture<CameraEditCameraComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CameraEditCameraComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraEditCameraComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
