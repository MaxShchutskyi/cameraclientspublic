import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder,FormControl, Validators } from '@angular/forms';
import {PlatesOpenModalWithVideosComponent} from
  "../../../plates-page/components/plates-open-modal-with-videos/plates-open-modal-with-videos.component";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {ipCameraExist, ipNameExist } from "../../validators/camera-exists-validator";
import {CameraManagerServiceService} from "../../services/camera-manager-service/camera-manager-service.service";
import {Camera} from "../../models/camera";

@Component({
  selector: 'app-camera-edit-camera-component',
  templateUrl: './camera-edit-camera-component.component.html',
  styleUrls: ['./camera-edit-camera-component.component.scss']
})
export class CameraEditCameraComponentComponent implements OnInit {
  frmGroup: FormGroup;
  copy:Camera;
  private pattern = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  constructor(private builder: FormBuilder, public  dialogRef: MatDialogRef<PlatesOpenModalWithVideosComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any, private cameraManagerServiceService: CameraManagerServiceService) {
    this.copy = Object.assign({}, this.data);
    this.frmGroup = this.builder.group({
      'id': new FormControl(''),
      'ip': new FormControl('', [Validators.required, Validators.pattern(this.pattern), ipCameraExist(this.cameraManagerServiceService.getAllCamerasFromCache().filter(x => x.ip !== this.copy.ip))]),
      'port': new FormControl('', Validators.required),
      'cameraName': new FormControl('', [Validators.required, ipNameExist(this.cameraManagerServiceService.getAllCamerasFromCache().filter(x => x.cameraName !== this.copy.cameraName))]),
      'login': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    });
  }
  ngOnInit() {
    if (!this.data) return;
    Object.keys(this.data).forEach(x => {
      var control = this.frmGroup.get(x);
      if (control)
        control.setValue(this.data[x]);
    });
  }
  saveChanges() {
    this.dialogRef.close(this.frmGroup.value);
  }
}
