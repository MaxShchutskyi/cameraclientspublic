import { Injectable } from '@angular/core';
import { HttpBaseService } from 'app/shared-services/http-base/http-base.service';
import {Camera} from "../../models/camera";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class CameraManagerServiceService {
  private cameras:Array<Camera> = [];
  constructor(private base:HttpBaseService) { }
  getAllCameras(): Observable<Array<Camera>> {
    this.cameras = [];
    return this.base.get("api/cameras").map(res => {
      this.cameras = res;
      return res;
    });
  }
  updateCamera(camera: Camera):Observable<Array<Camera>> {
    return this.base.put("api/cameras", camera).mergeMap(x => this.getAllCameras());
  }
  getAllCamerasFromCache() {
    return this.cameras;
  }
}
