import { TestBed, inject } from '@angular/core/testing';

import { CameraManagerServiceService } from './camera-manager-service.service';

describe('CameraManagerServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CameraManagerServiceService]
    });
  });

  it('should be created', inject([CameraManagerServiceService], (service: CameraManagerServiceService) => {
    expect(service).toBeTruthy();
  }));
});
