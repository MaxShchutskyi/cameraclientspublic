import { Injectable } from '@angular/core';
import {HttpBaseService} from "../../../../shared-services/http-base/http-base.service";
import {Page} from "../../models/page";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PlatesByFilterTableServiceService {

  constructor(private baseService: HttpBaseService) { }
  getTableData(page: Page): Observable<any> {
    var query = this.baseService.serialize(page);
    return this.baseService.get(`api/platetable/?${query}`);
  }
}
