import { TestBed, inject } from '@angular/core/testing';

import { PlatesByFilterTableServiceService } from './plates-by-filter-table-service.service';

describe('PlatesByFilterTableServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlatesByFilterTableServiceService]
    });
  });

  it('should be created', inject([PlatesByFilterTableServiceService], (service: PlatesByFilterTableServiceService) => {
    expect(service).toBeTruthy();
  }));
});
