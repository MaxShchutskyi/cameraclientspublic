import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatesOpenModalWithVideosComponent } from './plates-open-modal-with-videos.component';

describe('PlatesOpenModalWithVideosComponent', () => {
  let component: PlatesOpenModalWithVideosComponent;
  let fixture: ComponentFixture<PlatesOpenModalWithVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatesOpenModalWithVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatesOpenModalWithVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
