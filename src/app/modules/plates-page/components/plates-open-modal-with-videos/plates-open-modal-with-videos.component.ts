import { Component, OnInit,Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-plates-open-modal-with-videos',
  templateUrl: './plates-open-modal-with-videos.component.html',
  styleUrls: ['./plates-open-modal-with-videos.component.scss']
})
export class PlatesOpenModalWithVideosComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<PlatesOpenModalWithVideosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  ngOnInit() {
  }

}
