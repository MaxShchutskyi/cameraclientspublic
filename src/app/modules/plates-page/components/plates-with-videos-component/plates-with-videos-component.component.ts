import { Component, OnInit } from '@angular/core';
import {PlatesByFilterTableServiceService} from
  "../../services/plates-by-filter-service/plates-by-filter-table-service.service";
import { Page } from "../../models/page";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {PlatesOpenModalWithVideosComponent} from
  "../plates-open-modal-with-videos/plates-open-modal-with-videos.component";
import { Subject } from 'rxjs';
import * as moment from "moment"

@Component({
  selector: 'app-plates-with-videos-component',
  templateUrl: './plates-with-videos-component.component.html',
  styleUrls: ['./plates-with-videos-component.component.scss']
})
export class PlatesWithVideosComponentComponent implements OnInit {
  page: Page = new Page();
  modelChanged: Subject<string> = new Subject<string>();
  typesOfSelect: Array<any> = [{ name: 'All fields' }, { name: "Dates" }];
  dateFrom;
  dateTo;
  rows;
  constructor(private platesByFilterTableServiceService: PlatesByFilterTableServiceService, public dialog: MatDialog) {
    this.page.pageNumber = 0;
    this.page.size = 9;
    this.page.filterType = "All fields";
    this.modelChanged
      .debounceTime(800)
      .distinctUntilChanged()
      .subscribe(val => {
        console.log(this.page.filterValue);
        this.page.filterValue = val;
        this.page.changedFilterValue = true;
        this.page.dateFrom = "";
        this.page.dateTo = "";
        this.setPage({ offset: 0 });
      });
  }
  changeType() {
    this.page.filterValue = "";
    this.page.changedFilterValue = true;
    this.page.dateFrom = "";
    this.page.dateTo = "";
    this.setPage({ offset: 0 });
  }
  textChanged(text) {
    this.modelChanged.next(text);
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.page.changedPage = true;
    this.setData();
  }
  setData() {
    this.platesByFilterTableServiceService.getTableData(this.page).subscribe(res => {
      this.rows = res.dataRows;
      this.page.copyFields(res.page);
      this.page.cleanAllBooleanValues();
    });
  }
  openDialog(data) {
    this.dialog.open(PlatesOpenModalWithVideosComponent, {
      width: '600px',
      data: data
    });
  }
  fltr() {
    this.page.filterValue = "";
    this.page.changedFilterValue = true;
    this.page.dateFrom = moment(this.dateFrom).format();
    this.page.dateTo = moment(this.dateTo).format();
    debugger;
    this.setPage({ offset: 0 });
  }
  ngOnInit() {
    this.setPage({ offset: 0 });
  }

}
