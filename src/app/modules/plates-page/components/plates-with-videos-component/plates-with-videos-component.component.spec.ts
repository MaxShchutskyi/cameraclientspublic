import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatesWithVideosComponentComponent } from './plates-with-videos-component.component';

describe('PlatesWithVideosComponentComponent', () => {
  let component: PlatesWithVideosComponentComponent;
  let fixture: ComponentFixture<PlatesWithVideosComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatesWithVideosComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatesWithVideosComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
