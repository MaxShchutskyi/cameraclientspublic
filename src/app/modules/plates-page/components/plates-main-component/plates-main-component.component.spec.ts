import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatesMainComponentComponent } from './plates-main-component.component';

describe('PlatesMainComponentComponent', () => {
  let component: PlatesMainComponentComponent;
  let fixture: ComponentFixture<PlatesMainComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatesMainComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatesMainComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
