import { Component, OnInit } from '@angular/core';
import * as moment from "moment";
  import { Page } from 'app/modules/plates-page/models/page';
import { Subject } from 'rxjs';
import { PlatesByFilterTableServiceService } from 'app/modules/plates-page/services/plates-by-filter-service/plates-by-filter-table-service.service';
import { MatDialog } from '@angular/material';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-plates-main-component',
  templateUrl: './plates-main-component.component.html',
  styleUrls: ['./plates-main-component.component.scss']
})
export class PlatesMainComponentComponent implements OnInit {
  page: Page = new Page();
  modelChanged: Subject<string> = new Subject<string>();
  typesOfSelect: Array<any> = [{ name: 'All fields' }, { name: "Dates" }];
  dateFrom;
  dateTo;
  rows;
  host = environment.serverHost;
  constructor(private platesByFilterTableServiceService: PlatesByFilterTableServiceService, public dialog: MatDialog) {
    this.page.pageNumber = 0;
    this.page.size = 9;
    this.page.filterType = "All fields";
    this.modelChanged
      .debounceTime(800)
      .distinctUntilChanged()
      .subscribe(val => {
        console.log(this.page.filterValue);
        this.page.filterValue = val;
        this.page.changedFilterValue = true;
        this.page.dateFrom = "";
        this.page.dateTo = "";
        this.setPage({ offset: 0 });
      });
  }
  changeType() {
    this.page.filterValue = "";
    this.page.changedFilterValue = true;
    this.page.dateFrom = "";
    this.page.dateTo = "";
    this.setPage({ offset: 0 });
  }
  textChanged(text) {
    this.modelChanged.next(text);
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.page.changedPage = true;
    this.setData();
  }
  setData() {
    this.platesByFilterTableServiceService.getTableData(this.page).subscribe(res => {
      console.log(res);
      this.rows = res.dataRows;
      this.page.copyFields(res.page);
      this.page.cleanAllBooleanValues();
    });
  }
  fltr() {
    this.page.filterValue = "";
    this.page.changedFilterValue = true;
    this.page.dateFrom = moment(this.dateFrom).format();
    this.page.dateTo = moment(this.dateTo).format();
    this.setPage({ offset: 0 });
  }
  ngOnInit() {
    this.setPage({ offset: 0 });
  }
  //typesOfSelect: Array<any> = [{ name: 'All fields' }, { name: 'Camera' }, { name: 'Plate' }];
  //type: string = 'All fields';
  //rows;
  //constructor() {
  //  this.rows = [{ site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //    { site: 'St Louis Office', camera: 'Main', plate: 'KK6A6T', vehicle: 'Sray Silver Pontiak', confidence: '94.7', time: '12:34:59 am' },
  //  ];
  //}

  //ngOnInit() {
  //}

}
