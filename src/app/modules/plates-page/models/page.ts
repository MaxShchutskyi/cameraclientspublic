export class Page {
  pageNumber: number;
  size: number;
  totalElements: string;
  filterType: string;
  filterValue: string;
  dateFrom: any;
  dateTo: any;
  changedTypeOfFilter: boolean = false;
  changedPage: boolean = false;
  changedFilterValue: boolean = false;
  copyFields(obj) {
    Object.keys(obj).forEach(x => {
      this[x] = (!obj[x] || obj[x] === "null") ? null : obj[x];
    });
  }

  public cleanAllBooleanValues() {
    this.changedTypeOfFilter = this.changedPage = this.changedFilterValue = false;
  }
}
