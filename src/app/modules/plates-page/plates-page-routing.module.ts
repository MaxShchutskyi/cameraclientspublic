import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PlatesMainComponentComponent} from "./components/plates-main-component/plates-main-component.component";
import {PlatesWithVideosComponentComponent} from
  "./components/plates-with-videos-component/plates-with-videos-component.component";

const routes: Routes = [
  //{ path: '', component: PlatesWithVideosComponentComponent }
  { path: '', component: PlatesMainComponentComponent } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlatesPageRoutingModule { }
