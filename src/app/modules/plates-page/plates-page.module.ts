import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { PlatesPageRoutingModule } from './plates-page-routing.module';
import { PlatesMainComponentComponent } from './components/plates-main-component/plates-main-component.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { PopoverModule } from "ngx-popover";
import {PlatesWithVideosComponentComponent} from
  "./components/plates-with-videos-component/plates-with-videos-component.component";
import {PlatesByFilterTableServiceService} from
  "./services/plates-by-filter-service/plates-by-filter-table-service.service";
import { PlatesOpenModalWithVideosComponent } from './components/plates-open-modal-with-videos/plates-open-modal-with-videos.component';
import { MatDialogModule, MatDatepickerModule, MatNativeDateModule, MAT_DATE_LOCALE, MatFormFieldModule, MatInputModule } from '@angular/material';
@NgModule({
  imports: [
    CommonModule,
    NgSelectModule,
    MatDatepickerModule, MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    PopoverModule,
    NgxDatatableModule,
    MatDialogModule,
    FormsModule,
    PlatesPageRoutingModule
  ],
  declarations: [PlatesMainComponentComponent, PlatesWithVideosComponentComponent, PlatesOpenModalWithVideosComponent],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'en-GB' },PlatesByFilterTableServiceService],
  entryComponents: [PlatesOpenModalWithVideosComponent]
})
export class PlatesPageModule { }
