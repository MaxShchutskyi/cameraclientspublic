import { WebClientCamerasPage } from './app.po';

describe('web-client-cameras App', () => {
  let page: WebClientCamerasPage;

  beforeEach(() => {
    page = new WebClientCamerasPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
